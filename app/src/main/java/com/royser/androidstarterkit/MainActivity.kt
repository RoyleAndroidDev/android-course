package com.royser.androidstarterkit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnNext.setOnClickListener {
            if (!isPasswordValid(edtPassword.text)) {
                tilPassword.error = getString(R.string.shr_error_password)
            } else {
                tilPassword.error = null // Clear the error
                navigateToMenuList() // Navigate to the next Fragment
            }
        }

        tilPassword.setOnKeyListener { _, _, _ ->
            if (isPasswordValid(edtPassword.text)) {
                tilPassword.error = null //Clear the error
            }
            false
        }

    }

    private fun navigateToMenuList() {
        startActivity(Intent(this, MenuActivity::class.java))
        finish()
    }

    private fun isPasswordValid(text: Editable?): Boolean {
        return text != null && text.length >= 8
    }
}