package com.royser.androidstarterkit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_notification.*
import timber.log.Timber

class NotificationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        //TODO Notification & Firebase Cloud messaging 1.7 : create FirebaseInstanceId
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Timber.w("getInstanceId failed : " + task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                Toast.makeText(baseContext, "Token : $token", Toast.LENGTH_SHORT).show()
                Timber.d("Token : $token")
            })

        //TODO Notification & Firebase Cloud messaging 1.8 : create subscribeToTopic
        FirebaseMessaging.getInstance().subscribeToTopic("global")
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Timber.d("subscribeToTopic : global")
                }
            }


        btnAlertNotification.setOnClickListener {
            NotificationBuilder().createNotification(
                this,
                1,
                "Alert Notification from local",
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
            )
        }
    }
}