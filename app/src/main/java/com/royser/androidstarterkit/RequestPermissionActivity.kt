package com.royser.androidstarterkit

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_request_permission.*


class RequestPermissionActivity : AppCompatActivity(),
    //TODO Request permission 1.4 : implement PermissionListener
    PermissionListener {

    companion object {
        private const val REQUEST_IMAGE_CAPTURE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_permission)

        btnOpenCamera.setOnClickListener {
            checkCameraPermission()
        }

    }

    //TODO Request permission 1.8 : receive image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            imageView.setImageBitmap(imageBitmap)
        }
    }

    //TODO Request permission 1.3 : check permission
    private fun checkCameraPermission() {
        Dexter.withContext(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(this)
            .check()
    }

    //TODO Request permission 1.7 : open camera
    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
        //TODO Request permission 1.6 : open camera when Permission Granted
        if (response?.permissionName == Manifest.permission.CAMERA) {
            openCamera()
        }
    }

    override fun onPermissionRationaleShouldBeShown(
        permissionRequest: PermissionRequest?,
        permissionToken: PermissionToken?
    ) {
        permissionToken?.continuePermissionRequest()
    }

    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
        //TODO Request permission 1.5 : openPermissionSettings when Permission Denied
        if (response?.isPermanentlyDenied == true) {
            openPermissionSettings(this)
        }
    }

    private fun openPermissionSettings(activity: Activity) {
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + activity.packageName)
        )
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivity(intent)
    }
}
