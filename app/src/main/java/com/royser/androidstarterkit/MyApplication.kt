package com.royser.androidstarterkit

import android.app.Application
import timber.log.Timber

/**
 * Created by Royser on 24/6/2020 AD.
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }

}