package com.royser.androidstarterkit.ui.main

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.royser.androidstarterkit.R
import timber.log.Timber

class LifecycleFragment : Fragment() {

    companion object {
        fun newInstance() = LifecycleFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        Timber.d("onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        Timber.d("onCreateView")

        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        Timber.d("onActivityCreated")
    }

    override fun onStart() {
        super.onStart()

        Timber.d("onActivityCreated")
    }

    override fun onResume() {
        super.onResume()

        Timber.d("onResume")
    }

    override fun onPause() {
        super.onPause()

        Timber.d("onPause")
    }

    override fun onStop() {
        super.onStop()

        Timber.d("onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()

        Timber.d("onDestroyView")
    }

    override fun onDetach() {
        super.onDetach()

        Timber.d("onDetach")
    }

}