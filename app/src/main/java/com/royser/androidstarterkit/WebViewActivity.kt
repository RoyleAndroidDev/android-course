package com.royser.androidstarterkit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        webView.settings.javaScriptEnabled = true

        webView.settings.setSupportZoom(true)

        webView.settings.displayZoomControls = true
        webView.settings.builtInZoomControls = true

        webView.loadUrl("https://www.google.com")

    }
}