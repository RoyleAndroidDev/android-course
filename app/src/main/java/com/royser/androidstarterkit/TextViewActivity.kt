package com.royser.androidstarterkit

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import kotlinx.android.synthetic.main.activity_text_view.*

class TextViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text_view)

        //TODO Request TextView 1.0 : set text
        textView1.text = "TextView 1"

        //TODO Request TextView 1.1 : set html text
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView2.text = Html.fromHtml(
                "<h2>Title</h2><br><p>Description here</p>",
                Html.FROM_HTML_MODE_COMPACT
            )
        } else {
            textView2.text = Html.fromHtml(
                "<h2>Title</h2><br><p>Description here</p>"
            )
        }
    }
}