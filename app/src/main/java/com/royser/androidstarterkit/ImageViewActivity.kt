package com.royser.androidstarterkit

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_image_view.*

class ImageViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)

        //TODO Request ImageView 1.0 : setImageResource
        imageView2.setImageResource(R.drawable.shr_logo)

        //TODO Request ImageView 1.1 : display image from image url
//        imageView3.setImageURI(Uri.parse("https://www.android.com/static/2016/img/share/andy-lg.png"))

        //TODO Request ImageView 1.3 : display image from image url by Glide
        Glide
            .with(this)
            .load("https://www.android.com/static/2016/img/share/andy-lg.png")
            .into(imageView3)

    }
}