package com.royser.androidstarterkit.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.royser.androidstarterkit.R
import com.royser.androidstarterkit.model.ProductEntry
import kotlinx.android.synthetic.main.shr_product_card.view.*

/**
 * Adapter used to show a simple grid of products.
 */
class ProductCardRecyclerViewAdapter(private val productList: List<ProductEntry>) :
    RecyclerView.Adapter<ProductCardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductCardViewHolder {
        val layoutView =
            LayoutInflater.from(parent.context).inflate(R.layout.shr_product_card, parent, false)
        return ProductCardViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: ProductCardViewHolder, position: Int) {
        if (position < productList.size) {
            val product = productList[position]
            holder.itemView.tvProductTitle.text = product.title
            holder.itemView.tvProductPrice.text = product.price
            Glide
                .with(holder.itemView.context)
                .load(product.url)
                .into(holder.itemView.imvProduct)
        }
    }

    override fun getItemCount(): Int {
        return productList.size
    }
}
