package com.royser.androidstarterkit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.royser.androidstarterkit.adapter.ProductCardRecyclerViewAdapter
import com.royser.androidstarterkit.adapter.ProductGridItemDecoration
import com.royser.androidstarterkit.model.ProductEntry
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.menu_backdrop.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener(
            NavigationIconClickListener(
                this,
                nestedScrollView,
                AccelerateDecelerateInterpolator(),
                ContextCompat.getDrawable(this, R.drawable.shr_branded_menu), // Menu open icon
                ContextCompat.getDrawable(this, R.drawable.shr_close_menu) // Menu close icon
            )
        )

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        val adapter = ProductCardRecyclerViewAdapter(
            ProductEntry.initProductEntryList(resources)
        )
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(
            ProductGridItemDecoration(
                resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing),
                resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small)
            )
        )

        btnActivity.setOnClickListener {
            startActivity(Intent(this, LifeCycleActivity::class.java))
        }

        btnFragment.setOnClickListener {
            startActivity(Intent(this, LifecycleFragmentActivity::class.java))
        }

        btnTextView.setOnClickListener {
            startActivity(Intent(this, TextViewActivity::class.java))
        }

        btnEditText.setOnClickListener {
            startActivity(Intent(this, EditTextActivity::class.java))
        }

        btnImageView.setOnClickListener {
            startActivity(Intent(this, ImageViewActivity::class.java))
        }

        btnNotification.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
        }

        btnRequestPermission.setOnClickListener {
            startActivity(Intent(this, RequestPermissionActivity::class.java))
        }

        btnWebView.setOnClickListener {
            startActivity(Intent(this, WebViewActivity::class.java))
        }
    }
}