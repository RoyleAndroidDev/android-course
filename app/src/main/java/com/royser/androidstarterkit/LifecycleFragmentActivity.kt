package com.royser.androidstarterkit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.royser.androidstarterkit.ui.main.LifecycleFragment
import timber.log.Timber

class LifecycleFragmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("onCreate")

        setContentView(R.layout.lifecycle_fragment_activity)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, LifecycleFragment.newInstance())
                .commitNow()
        }
    }

    override fun onRestart() {
        super.onRestart()

        Timber.d("onRestart")
    }

    override fun onStart() {
        super.onStart()

        Timber.d("onStart")
    }

    override fun onResume() {
        super.onResume()

        Timber.d("onResume")
    }

    override fun onPause() {
        super.onPause()

        Timber.d("onPause")
    }

    override fun onStop() {
        super.onStop()

        Timber.d("onStop")
    }

    override fun onDestroy() {
        super.onDestroy()

        Timber.d("onDestroy")
    }
}