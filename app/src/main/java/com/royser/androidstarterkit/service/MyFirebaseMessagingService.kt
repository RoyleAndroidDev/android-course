package com.royser.androidstarterkit.service

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.royser.androidstarterkit.NotificationBuilder
import timber.log.Timber

/**
 * Created by Royser on 27/6/2020 AD.
 */
//TODO Notification & Firebase Cloud messaging 1.4 : Create FirebaseMessagingService
class MyFirebaseMessagingService : FirebaseMessagingService() {

    companion object {
        const val TAG = "TAG"
    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)

        Timber.d("Refreshed token: $newToken")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(newToken)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Timber.d("From: %s", (remoteMessage.from))

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Timber.d("Message data payload: %s", remoteMessage.data)

            NotificationBuilder().createNotification(
                this,
                remoteMessage.data["notificationId"]?.toInt() ?: 0,
                remoteMessage.data["title"],
                remoteMessage.data["body"]
            )

        }

        // Check if message contains a notification payload.
//        remoteMessage.notification?.let {
//            Timber.d("Message Notification Body: %s", remoteMessage.notification?.body)
//
//            NotificationBuilder().createNotification(
//                this,
//                remoteMessage.notification?.title,
//                remoteMessage.notification?.body
//            )
//        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

    }

    private fun sendRegistrationToServer(token: String?) {

    }

}