package com.royser.androidstarterkit

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import java.util.*

/**
 * Created by Royser on 27/6/2020 AD.
 */

//TODO Notification & Firebase Cloud messaging 1.6 : create NotificationBuilder
class NotificationBuilder {

    fun createNotification(
        context: Context,
        notificationId: Int,
        title: String?,
        content: String?
    ) {

        // Create an explicit intent for an Activity in your app
        val intent = Intent(context, MenuActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val mChannel = NotificationChannel(
                context.resources.getString(R.string.default_notification_channel_id),
                "notification_channel_name",
                NotificationManager.IMPORTANCE_HIGH
            )
            mChannel.description = "notification_channel_description"
            mChannel.enableVibration(true)
            mChannel.enableLights(true)
            mChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC

            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager =
                context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)

        }

        val builder = NotificationCompat.Builder(
            context,
            context.resources.getString(R.string.default_notification_channel_id)
        )
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(content)
            .setDefaults(Notification.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

//BigTextStyle
//            .setStyle(NotificationCompat.BigTextStyle().bigText(content))

//LargeIcon + BigPictureStyle
//            .setLargeIcon(getBitmap(context,"https://www.android.com/static/2016/img/share/andy-lg.png"))
//            .setStyle(NotificationCompat.BigPictureStyle()
//                .bigPicture(getBitmap(context,"https://www.android.com/static/2016/img/share/andy-lg.png"))
//                .bigLargeIcon(null))

        with(NotificationManagerCompat.from(context)) {
            // notificationId is a unique int for each notification that you must define
            notify(
                notificationId,
                builder.build()
            )
        }

    }

    private fun getBitmap(context: Context, imgUrl: String): Bitmap? {
        var bitmap: Bitmap? = null

        Glide.with(context)
            .asBitmap()
            .load(imgUrl)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    bitmap = resource
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    // this is called when imageView is cleared on lifecycle call or for
                    // some other reason.
                    // if you are referencing the bitmap somewhere else too other than this imageView
                    // clear it here as you can no longer have the bitmap
                }
            })

        return bitmap
    }

}